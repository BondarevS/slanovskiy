$( document ).ready( function () {

    function floatingHeader () {
        let header = $( '#header' ),
            stickyOffset = header.height();

        console.log( stickyOffset );

        $( window ).scroll( function () {
            if ( $(window).scrollTop() > stickyOffset) {
                header.addClass("sticky");
            } else {
                header.removeClass("sticky");
            }
        } );
    }
    floatingHeader();

    enquire.register( "screen and (min-width:1024px)", {
            match: function () {
                $( '.nav_left' ).insertBefore( '.logo' );
                $( '.nav_right' ).insertAfter( '.logo' );
            },
            unmatch: function () {
                $( '.nav_left' ).appendTo( '#j-navWrap' );
                $( '.nav_right' ).insertAfter( '.nav_left' );
            }
        } );

    (function initMenu () {
            /*=== Open/close nav menu in mobile ===*/
            $( ".btnToggle" ).on( 'click', function () {
                var $menu = $( '.nav-mobile' ),
                    $firstLine = $( '.first-line' ),
                    $lastLine = $( '.last-line' );


                if ( $menu.hasClass( 'open' ) ) {
                    $menu.removeClass( 'open' );
                    $firstLine.removeClass( 'rotateUpLeft' ).addClass( 'rotateUpLeft_inv' );
                    $lastLine.removeClass( 'rotateDownLeft' ).addClass( 'rotateDownLeft_inv' );
                } else {
                    $menu.addClass( 'open' );
                    $firstLine.removeClass( 'rotateUpLeft_inv' ).addClass( 'rotateUpLeft' );
                    $lastLine.removeClass( 'rotateDownLeft_inv' ).addClass( 'rotateDownLeft' );
                }
            });

            /*===  Close nav menu on click out menu ===*/
            $( document ).on( 'click', function ( e ) {
                var $openedMenu = $( '.nav-mobile.open' );
                if ( 0 < $openedMenu.length  && false === $openedMenu.is( e.target ) && false === $( '.btnToggle' ).is( e.target ) && false === $( '.btnToggle__line' ).is( e.target ) ){
                    $( '.nav-mobile' ).removeClass( 'open' );
                    $( '.first-line' ).removeClass( 'rotateUpLeft' ).addClass( 'rotateUpLeft_inv' );
                    $( '.last-line' ).removeClass( 'rotateDownLeft' ).addClass( 'rotateDownLeft_inv' );
                }
            } );
        })();



        // wow = new WOW(
        //     {
        //         boxClass:     'wow',      // default
        //         animateClass: 'animated', // default
        //         offset:       0,          // default
        //         mobile:       true,       // default
        //         live:         true        // default
        //     }
        // );

        // wow.init();

    let bookingForm = $("#booking-form");
    if ( bookingForm.length > 0 ) {
        let $fields = bookingForm.find( 'input[type=text], input[type=tel], input[type=email]' );

        $fields.on( 'change', function () {
            // console.log( $(this) );
            // console.log( $(this).val() );

            if ( $(this).val() !== "" ) {
                console.log( "change state" );
                $(this).next("label").addClass("field-dirty");
            } else {
                $(this).next("label").removeClass("field-dirty");
            }
        });
    }


    let saleSlider = $( '.sale-slider' );
    if ( saleSlider.length > 0 ) {
        saleSlider.slick({
            infinite: false,
            slidesToShow: 3,
            slidesToScroll: 1,
            arrows: false,
            dots: true,
            responsive: [
                {
                    breakpoint: 768,
                    settings: {
                        slidesToShow: 2
                    }
                },
                {
                    breakpoint: 480,
                    settings: {
                        slidesToShow: 1
                    }
                }
            ]
        });
    }

    let commentsSlider = $( ".comments-slider" );
    if ( commentsSlider.length > 0 ) {
        commentsSlider.slick({
            infinite: false,
            slidesToShow: 1,
            slidesToScroll: 1,
            arrows: false,
            dots: true
        });
    }


    //Sliders for single page
    let productsGallery = $('.products__gallery')
    if ( productsGallery.length > 0 ) {
        $('.slider-for').slick({
            infinite: false,
            slidesToShow: 1,
            slidesToScroll: 1,
            arrows: false,
            // fade: true,
            asNavFor: '.slider-nav'
        });
        $('.slider-nav').slick({
            infinite: false,
            slidesToShow: 3,
            slidesToScroll: 1,
            asNavFor: '.slider-for',
            dots: false,
        });
    }
});