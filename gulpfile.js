var gulp        =   require( 'gulp' ),
    watch       =   require( 'gulp-watch' ),
    /*-----CSS-----*/
    sass        =   require( 'gulp-sass' ),
    prefixer    =   require( 'gulp-autoprefixer' ),
    sourcemaps  =   require( 'gulp-sourcemaps' ),
    cssmin      =   require( 'gulp-cssmin' ),
    /*-----Fonts-----*/
    ttf2eot     =   require( 'gulp-ttf2eot' ),
    ttf2woff    =   require( 'gulp-ttf2woff' ),
    ttf2woff2   =   require( 'gulp-ttf2woff2' ),
    ttf2svg     =   require( 'gulp-ttf-svg' ),
    iconfont    =   require( 'gulp-iconfont' ),
    iconfontCss =   require( 'gulp-iconfont-css' ),
    /*-----Images-----*/
    imagemin    =   require( 'gulp-imagemin' ),
    pngquant    =   require( 'imagemin-pngquant' ),
    /*-----JS-----*/
    concat      =   require( 'gulp-concat' ),
    uglify      =   require( 'gulp-uglify' ),
    babel       =   require( 'gulp-babel' ),
    /*-----Other-----*/
    del         =   require( 'del' ),
    rev_append        =   require( 'gulp-rev-append' );


var runTimestamp = Math.round( Date.now() / 1000 ),
    fontName = 'icons';


var path = {
    build: {
        css     :   'build/css/',
        images  :   'build/images/',
        fonts   :   'build/fonts/',
        js      :   'build/js/'
    },
    src: {
        css     :   'assets/scss/*.scss',
        images  :   'assets/images/*.{jpg,png,svg,ico,gif}*',
        fonts   :   'assets/fonts/',
        icons   :   'assets/icons/*.svg',
        js      :   'assets/js/*.js'
    },
    watch: {
        css     :   'assets/scss/**/*.scss',
        js      :   'assets/js/*.js'
    }
};


/*===== Task for CSS =====*/
gulp.task( 'style', function () {
    gulp.src( path.src.css )
        .pipe( sourcemaps.init() )
        .pipe( sass(  ) )
        .pipe( prefixer( {browsers: 'last 2 versions' } ) )
        .pipe( sourcemaps.write( 'maps' ) )
        .pipe( gulp.dest( path.build.css ) );
});


/*===== Tasks for FONTS =====*/
gulp.task( 'ttf2eot', function() {
    gulp.src( path.src.fonts + '*.ttf' )
        .pipe( ttf2eot() )
        .pipe( gulp.dest( path.build.fonts ) );
});


gulp.task( 'ttf2woff', function() {
    gulp.src( path.src.fonts + '*.ttf' )
        .pipe( ttf2woff() )
        .pipe( gulp.dest( path.build.fonts ) );
});


gulp.task( 'ttf2woff2', function() {
    gulp.src( path.src.fonts + '*.ttf' )
        .pipe( ttf2woff2() )
        .pipe( gulp.dest( path.build.fonts ) );
});


gulp.task( 'ttf2svg', function(){
    gulp.src( path.src.fonts + '*.ttf' )
        .pipe( ttf2svg() )
        .pipe( gulp.dest( path.build.fonts ) );
});


gulp.task( 'iconfont', function() {
    gulp.src( path.src.icons )
        .pipe( iconfontCss({
                               path: 'assets/scss/icons/templates/_icons.scss',
                               fontName: fontName,
                               targetPath: '../scss/icons/_icons.scss',
                               fontPath: '../fonts/',
                               timestamp: runTimestamp
                           }) )
        .pipe( iconfont({
                            fontName: fontName,
                            timestamp: runTimestamp,
                            formats: ['svg', 'ttf', 'eot', 'woff', 'woff2'],
                            normalize: true,
                            fontHeight: 1001
                        }))
        .pipe( gulp.dest( path.src.fonts ) );
});


gulp.task( 'fonts', function() {
    gulp.src( path.src.fonts + '*.*' )
        .pipe( gulp.dest( path.build.fonts ) );
});


/*===== Tasks for images =====*/
gulp.task( 'images', function () {
    gulp.src( path.src.images )
        .pipe( imagemin({
                            progressive: true,
                            svgoPlugins: [ {removeViewBox: false} ],
                            use: [ pngquant() ],
                            interlaced: true
                        }) )
        .pipe( gulp.dest( path.build.images ) );
});


/*===== Task for JS =====*/
gulp.task( 'js', function () {
    gulp.src( path.src.js )
        .pipe( concat( 'main.js' ) )
        .pipe( sourcemaps.init() )
        .pipe(babel({
                        presets: ['@babel/env']
                    }))

        .pipe( sourcemaps.write() )
        .pipe( gulp.dest( path.build.js ) );
});



/*===== Tasks for libs =====*/
gulp.task( 'scripts-js', function () {
    gulp.src( [
                  'node_modules/jquery/dist/jquery.min.js',
                  'node_modules/slick-carousel/slick/slick.min.js',
                  'node_modules/enquire.js/dist/enquire.min.js'
              ] )
        .pipe( gulp.dest( path.build.js) );
});

gulp.task( 'scripts-css', function () {
    gulp.src( [
                  'node_modules/slick-carousel/slick/slick.css'
              ] )
        .pipe( cssmin() )
        .pipe( gulp.dest( path.build.css ) );
});

gulp.task( 'rev', function() {
    gulp.src( './*.html' )
        .pipe( rev_append() )
        .pipe( gulp.dest( '.' ) );
});



gulp.task( 'build', [
    // 'ttf2woff',
    // 'ttf2woff2',
    // 'ttf2eot',
    // 'ttf2svg',
    // 'iconfont',
    // 'fonts',
    // 'images',
    // 'scripts-css',
    // 'scripts-js',
    'style',
    'js'
] );


gulp.task( 'watch', function () {
    gulp.watch( path.watch.css, function(event, cb) {
        gulp.start( 'style' );
    });
    gulp.watch( path.watch.js, function(event, cb) {
        gulp.start( 'js' );
    });
});


gulp.task( 'default', [ 'build' ], function () {
    gulp.start( 'watch' );
});



/*=========================== Production =====================================*/

gulp.task('clean', function() {
    return del.sync( ['build/js/*', '!build/js/*.min.js'] );
});

gulp.task( 'prod-js', function () {
    gulp.src( path.src.js )
        .pipe( concat( 'main.min.js' ) )
        .pipe(babel({
                        presets: ['@babel/env']
                    }))
        .pipe( uglify() )
        .pipe( gulp.dest( path.build.js ) );
});




gulp.task('prod', [
    'ttf2woff',
    'ttf2woff2',
    'ttf2eot',
    'ttf2svg',
    'iconfont',
    'fonts',
    'images',
    'scripts-css',
    'scripts-js',
    // 'rev',
    'style',
    'prod-js',
    'clean'
] );